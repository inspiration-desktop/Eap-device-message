import { IStoreState } from '@/interfaces/store.interface';
import Vuex from 'vuex';
import Vue from 'vue';

Vue.use(Vuex);

const state: IStoreState = {
  token: '',
  srand: '',
  accountInfo: {
    // 当前登录用户信息
    acoid: '',
    nickname: '',
    profile: ''
  },
  permissions: null
};

process.env.NODE_ENV === 'development'
  ? (state.accountInfo = {
    // 前端模拟多人测试
    acoid: +new Date() + Math.random() + '',
    nickname: +new Date() + '',
    profile: ''
  })
  : (state.accountInfo = {
    // 当前登录用户信息
    acoid: '',
    nickname: '',
    profile: ''
  });

export default new Vuex.Store({
  state,
  mutations: {
    update(state, data) {
      Object.keys(data).forEach((key: string) => {
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        (state as any)[key] = data[key];
      });
    }
  }
});
