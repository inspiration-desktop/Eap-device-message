export interface IKeyValue {
  key: string;
  value: any;
}

export interface IResponse {
  result: boolean,
  message: string,
  data?: any,
}

export interface ISafeAreaData {
  top: number;
  left: number;
  right: number;
  bottom: number;
}

export type IDeviceCategory = 'device' | 'lora' | 'mqtt' | 'coap'