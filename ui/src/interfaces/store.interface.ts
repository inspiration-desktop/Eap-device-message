import { EdgerPermissionResult } from '@edgeros/web-sdk';
import { IUser } from './auth.interface';

export interface IStoreState {
  srand: string;
  token: string;
  accountInfo: IUser; // 当前登录账号信息
  permissions: EdgerPermissionResult | null
}

export interface IClickMusic {
  status: boolean; // 点击效果是否运行
  clickStamp?: number; // 点击时间戳，触发播放
}
