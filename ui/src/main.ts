import Vue from 'vue';
import './registerServiceWorker';
import router from './router';
import store from './store';
import 'normalize.css';
import { edger } from '@edgeros/web-sdk';
import { initSocket } from './services/socket';
import 'vant/lib/index.less';
import App from './App';

Vue.config.productionTip = false;

Vue.prototype.$edger = edger;

edger.avoidKeyboardOverlay();


edger.token().then((data) => {
  if (data) {
    store.commit('update', { token: data.token, srand: data.srand });
    initSocket()
  }
});

edger.onAction('token', (data) => {
  if (data) {
    store.commit('update', { token: data.token, srand: data.srand });
  }
});

edger.permission
  .request({
    code: ['lora', 'network', 'coap', 'mqtt.publish', 'mqtt.subscribe'],
    type: 'permissions'
  })
  .catch(() => {
    edger.notify.error('授权失败！');
  })

edger.onAction('permission', data => {
  console.log('permission onAction: ', data);
  store.commit('update', { permissions: data });
});

edger.permission
  .fetch()
  .then((data) => {
    console.log('edger.permission.fetch(): ', data);
    store.commit('update', { permissions: data });
  })
  .catch((error) => {
    console.error(error);
  });

edger
  .user()
  .then((result) => {
    console.log('edger.user(): ', result);
    store.commit('update', { accountInfo: result });
  })
  .catch((error: Error) => {
    throw error;
  })

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');
