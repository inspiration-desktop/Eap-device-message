import { Vue, Component } from 'vue-property-decorator';
import '@/styles/animation.less';
import style from '@/styles/home.module.less';
import deviceStyle from '@/styles/device.module.less';
import { Empty, Tab, Tabs } from 'vant';
import { Coap, Device, Lora, Mqtt } from '@/components';

@Component({
  sockets: {
    connect() {
      // 说明是断线重连
      this.$edger.permission
        .fetch()
        .then(data => {
          console.log('permission fetch: ', data);
          this.$store.commit('update', { permissions: data });
          this.$forceUpdate();
        })
        .catch(error => {
          console.error(error);
        });
    }
  },

  components: {
    'van-tabs': Tabs,
    'van-tab': Tab,
    'van-empty': Empty,
    device: Device,
    lora: Lora,
    coap: Coap,
    mqtt: Mqtt
  }
})
export default class Home extends Vue {
  private deviceType: 'Device' | 'Lora' | 'Coap' | 'Mqtt' = 'Coap';

  public render() {
    return (
      <div class={style['home']}>
        <h2>设 备 通 信</h2>
        <van-tabs
          v-model={this.deviceType}
          background='#1180b8'
          color='#fff'
          title-active-color='#fff'
          title-inactive-color='#20a5de'>
          <van-tab title='Coap' name='Coap'></van-tab>
          <van-tab title='Sddc/Zddc' name='Device'></van-tab>
          <van-tab title='Lora' name='Lora'></van-tab>
          <van-tab title='Mqtt' name='Mqtt'></van-tab>
        </van-tabs>
        <div class={style['home-container']}>
          {this.deviceType === 'Device' && <device></device>}
          {this.deviceType === 'Lora' && <lora></lora>}
          {this.deviceType === 'Coap' && <coap></coap>}
          {this.deviceType === 'Mqtt' && <mqtt></mqtt>}
        </div>

        {/* socket网络 */}
        {/* {(!this.$socket || (this.$socket && this.$socket.disconnected)) && (
          <div class={deviceStyle['permission']}>
            <van-empty image='network' description='网络中断，重新连接中...' />
          </div>
        )} */}
      </div>
    );
  }
}
