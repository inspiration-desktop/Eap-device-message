declare module '*.module.less' {
  const less: any;
  export default less
}

declare module '*.png' {
  const png: any;
  export default png
}

declare module '*.svg' {
  const svg: any;
  export default svg
}