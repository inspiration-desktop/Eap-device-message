import Vue from 'vue';
import VueRouter, { RouteConfig } from 'vue-router';
import Home from '@/views/Home';

Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
  {
    path: '/',
    name: 'home',
    component: Home,
  }
];

const router = new VueRouter({
  routes,
});

// router.beforeEach((to: Route, from: Route, next) => {
//   if (!to.meta || !from.meta) {
//     throw new Error('路由参数meta出错。');
//   } else {
//     if (to.meta.index > from.meta.index) {
//       store.commit('update', { transitionName: 'slide-left' });
//     } else {
//       store.commit('update', { transitionName: 'slide-right' });
//     }
//   }
//   next();
// });

export default router;
