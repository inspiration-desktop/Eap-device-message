export interface LoRaDeviceInfo {
  alias: string; // LoRa device alias.
  devEUI: string; // LoRa device EUI.
}
